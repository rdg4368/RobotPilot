*** Settings ***

Documentation           Principal
Library                 SeleniumLibrary


*** Variable ***

${URL}                  http://www.google.com
${NAVEGADOR}            firefox


*** Keywords ***

Abrir Navegador         ${URL}  ${NAVEGADOR}
    Open Browser
    Maximize Browser de Privacidade
    Aceitar Termo de Privacidade

Aceitar Term de Privacidade
    Wait Until Element Is Visible
    Click Button

Capturar tela
    Capturar Screenshot

Fechar Navegador
    Sleep
    Close Browser