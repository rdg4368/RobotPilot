*** Settings ***
Documentation Suite de teste acesso ao site da caixa

Resource    ${EXECDIR}/resources/Base.robot
Resource    ../resources/AcessarMinhaConta.robot
Resource    ../resources/Acessibilidade.robot
Resource    ../resources/Loterias.robot

Test Setup      Abrir Navegador
Test Teardown   Fechar Navegador

*** Test Case ***

Cenario: Acessar Minha Conta
    Acessar Minha Conta
    Ir para Conteudo
    Capturar Screenshot